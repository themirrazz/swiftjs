function compileSwiftRough(swift) {
    return ` ${swift} `.replaceAll(
        ' let ',' const '
    ).replaceAll(
        '{let ','{const '
    ).replaceAll(
        '\nlet ','\nconst '
    ).replaceAll(
        ' func ', ' function '
    ).replaceAll(
        '{func ','{function '
    ).replaceAll(
        '\nfunc ','\nfunction '
    )
}
