# SwiftJS
Compile Apple Swift code, to JavaScript, in JavaScript!

## Different Versions

**full-compile.js** is a more complete version (not fully complete yet), that takes up for room.<br>
**full-compile.min.js** is a minifed version of the "full" version.<br>
**rough-compile.js** is a more light-weight version.